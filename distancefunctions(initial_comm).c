#include<stdio.h>
#include<math.h>
int distance(int,int,int,int);
int main()
{
    int a,b,c,d;
    float dis;
    printf("Enter the coordinates of the first point\n");
    scanf("%d%d",&a,&b);
    printf("Enter the coordinates of the second point\n");
    scanf("%d%d",&c,&d);
    dis= distance(a,b,c,d);
    printf("Distance between the two points is %f\n",dis);
    return 0;
}
int distance(int a,int b,int c, int d)
{
    return sqrt(((a-c)*(a-c))+((b-d)*(b-d)));
}
