#include<stdio.h>
#include<math.h>
float dis(int x1,int y1,int x2,int y2);
int main()
{
    int x1,x2,y1,y2;
    float dis;
    printf("Enter the coordinates of the first point \n");
    scanf("%d%d",&x1,&y1);
    printf("Enter the coordinates of the second point \n");
    scanf("%d%d",&x2,&y2);
    dis=dis(x1,x2,y1,y2);
    printf("The distance is %.2f\n",dis);
    return 0;
}
float dis(int x1,int y1,int x2,int y2)
{
    return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}