#include<stdio.h>
#include<math.h>
int input()
{
    int a;
    printf("Enter the values of the coordinates of point one and point two one by one\n in the format x1,y1  x2,y2\n");
    scanf("%d",&a);
    return a;
}
float compute(int x1,int y1,int x2,int y2)
{
    return sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
}
void output(int x1,int x2,int y1,int y2,float dis)
{
    printf("The distance between the points (%d,%d) and (%d,%d) is %f\n",x1,y1,x2,y2,dis);
}
int main()
{
    int x1,y1,x2,y2;
    float dis;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    dis=compute(x1,y1,x2,y2);
    output(x1,y1,x2,y2,dis);
    return 0;
}
