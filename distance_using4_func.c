#include<stdio.h>
#include<math.h>
int input()
{
    int a;
    printf("Enter the values of the coordinates of point one and point two one by one\n in the format x1,y1  x2,y2\n");
    scanf("%d",&a);
    return a;
}
int compute(int a,int b,int c,int d)
{
    return sqrt((c-a)*(c-a)+(d-b)*(d-b));
}
void output(int a,int b,int c,int d,float dis)
{
    printf("The distance between the points (%d,%d) and (%d,%d) is %f\n",a,c,b,d,dis);
}
int main()
{
    int a,b,c,d;
    float dis;
    a=input();
    b=input();
    c=input();
    d=input();
    dis=compute(a,b,c,d);
    return 0;
}