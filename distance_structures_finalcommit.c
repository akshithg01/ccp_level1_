#include<stdio.h>
#include<math.h>
float dist(int x1,int x2, int y1, int y2);
struct distance
{
    int x1,x2,y1,y2;
    float dis;
}s;
float dist(int x1,int x2,int y1,int y2)
{
    return sqrt(pow((s.x2-s.x1),2)+pow((s.y2-s.y1),2));
}
int main()
{
    printf("Enter the coordinates of the first point \n ");
    scanf("%d%d",&s.x1,&s.y1);
    printf("Enter the coordinates of the second point \n");
    scanf("%d%d",&s.x2,&s.y2);
    s.dis = dist(s.x1,s.x2,s.y1,s.y2);
    printf("The distance between the two points is %.2f\n",s.dis);
    return 0;
}